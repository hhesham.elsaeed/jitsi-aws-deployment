terraform {
  required_version = ">= 0.12"
  backend "s3" {
    key = "vpc-peering/terraform.tfstate"
  }
  required_providers {
    sops = "~> 0.5"
  }
}

data "terraform_remote_state" "vpc_main" {
  backend = "s3"
  config = {
    bucket = "${local.remote_state_bucket}"
    key    = "vpc-main/terraform.tfstate"
  }
}

data "terraform_remote_state" "vpc_satellites" {
  backend = "s3"
  config = {
    bucket = "${local.remote_state_bucket}"
    key    = "vpc-satellites/terraform.tfstate"
  }
}
