SELF_DIR := $(dir $(lastword $(MAKEFILE_LIST)))
include $(SELF_DIR)/../Makefile.env
ROOTS         = setup letsencrypt-certs vpc-main vpc-satellites vpc-peering jitsi-master jitsi-videobridges monitoring
SHELL         = bash
ROOTS_DESTROY = $(patsubst %, destroy/%, $(call _reverse, $(ROOTS)))
ROOTS_NUKE = $(patsubst %, nuke/%, $(call _reverse, $(ROOTS)))
_reverse      = $(shell printf "%s\n" $(strip $1) | tac)

.PHONY: $(ROOTS) $(ROOTS_DESTROY)

$(ROOTS):
	@cd $@ && make init && make apply

$(ROOTS_DESTROY):
	cd $(@F) && make init destroy

$(ROOTS_NUKE):
	cd $(@F) && make init nuke


apply-all: TERRAFORM_ARGS = -auto-approve
apply-all: backend.tfvars
	export TERRAFORM_ARGS=$(TERRAFORM_ARGS) && $(MAKE) setup
	export TERRAFORM_ARGS=$(TERRAFORM_ARGS) && $(MAKE) vpc-main
	export TERRAFORM_ARGS=$(TERRAFORM_ARGS) && $(MAKE) vpc-satellites
	export TERRAFORM_ARGS=$(TERRAFORM_ARGS) && $(MAKE) vpc-peering
	export TERRAFORM_ARGS=$(TERRAFORM_ARGS) && $(MAKE) letsencrypt-certs
	export TERRAFORM_ARGS=$(TERRAFORM_ARGS) && $(MAKE) ansible
	export TERRAFORM_ARGS=$(TERRAFORM_ARGS) && $(MAKE) build-ami-debian-base
	export TERRAFORM_ARGS=$(TERRAFORM_ARGS) && $(MAKE) jitsi-master
	export TERRAFORM_ARGS=$(TERRAFORM_ARGS) && $(MAKE) build-ami-jvb
	export TERRAFORM_ARGS=$(TERRAFORM_ARGS) && $(MAKE) jitsi-videobridges

destroy-all: backend.tfvars
	$(MAKE) destroy/jitsi-videobridges
	$(MAKE) destroy/jitsi-master
	$(MAKE) destroy/letsencrypt-certs
	$(MAKE) destroy/vpc-peering
	$(MAKE) destroy/vpc-satellites
	$(MAKE) destroy/vpc-main
	$(MAKE) destroy/setup

nuke-all: backend.tfvars
	$(MAKE) nuke/jitsi-videobridges

build-ami-debian-base:
	cd ../packer && $(MAKE) debian-base

build-ami-prometheus:
	cd ../packer && $(MAKE) prometheus

build-ami-jvb:
	cd ../packer && $(MAKE) jvb

ansible:
	cd ../../ansible && $(MAKE) deploy

fmt:
	terraform fmt --recursive

check-clean:
	@echo  "This action is irreversible and will erase local terraform state, without destroying any resources (remote state will still exist)."
	@echo -n "Are you sure? [y/N] " && read ans && [ $${ans:-N} = y ]

dist-clean: check-clean
	@echo "WARNING: Killing all .terraform dirs and backend.tfvars in 5 seconds..."
	@sleep 5
	@find . -mindepth 1 -type d -iname ".terraform" | xargs rm -r
	@rm -f backend.tfvars
	@echo "done"


backend.tfvars:
	cd remote-state && $(MAKE) setup
