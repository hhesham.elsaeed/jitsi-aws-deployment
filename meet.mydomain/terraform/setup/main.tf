terraform {
  required_version = ">= 0.12"

  backend "s3" {
    key = "setup/terraform.tfstate"
  }
  required_providers {
    sops = "~> 0.5"
  }
}

provider "aws" {}

resource "aws_s3_account_public_access_block" "block_public_s3" {
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

module "ssm_setup" {
  source        = "git::https://gitlab.com/guardianproject-ops/terraform-aws-ssm-ansible-setup.git?ref=master"
  namespace     = local.namespace
  stage         = local.stage
  name          = local.name
  force_destroy = true
}


