{
  "builders": [
{{ range $index, $region := (ds "config").regions }}
    {
      "name": "debian-base-{{ $region }}",
      "type": "amazon-ebs",
      "region": "{{ $region }}",
      "source_ami_filter": {
        "filters": {
          "virtualization-type": "hvm",
          "name": "{{(ds "config").ami_filter_name}}",
          "root-device-type": "ebs"
        },
        "owners": ["{{(ds "config").ami_filter_owner}}"],
        "most_recent": true
      },
      "instance_type": "t3.small",
      "temporary_iam_instance_profile_policy_document": {{(ds "config").temporary_iam_instance_profile_policy_document}},
      "ssh_username": "admin",
      "ami_name": "base-debian-{{env.ExpandEnv "${LABEL_NAMESPACE}-${LABEL_STAGE}"}}-{{(ds "config").debian_codename}}",
      "ami_groups": ["all"],
      "run_tags": {
        "Namespace": "{{getenv "LABEL_NAMESPACE"}}",
        "Stage": "{{getenv "LABEL_STAGE"}}",
        "Project": "{{(ds "config").project}}"
      },
      "run_volume_tags": {
        "Project": "{{(ds "config").project}}",
        "Namespace": "{{getenv "LABEL_NAMESPACE"}}",
        "Stage": "{{getenv "LABEL_STAGE"}}"
      },
      "tags": {
        "Name": "debian-base-{{env.ExpandEnv "${LABEL_NAMESPACE}-${LABEL_STAGE}"}}",
        "Date": "{{"{{"}}isotime | clean_resource_name{{"}}"}}",
        "OS": "debian/{{(ds "config").debian_codename}}",
        "Project": "{{(ds "config").project}}",
        "Namespace": "{{getenv "LABEL_NAMESPACE"}}",
        "Stage": "{{getenv "LABEL_STAGE"}}",
        "Application": "debian-base"
      }
    },
    {
      "name": "jvb-{{ $region }}",
      "type": "amazon-ebs",
      "region": "{{ $region }}",
      "source_ami_filter": {
        "filters": {
          "virtualization-type": "hvm",
          "name": "base-debian-{{env.ExpandEnv "${LABEL_NAMESPACE}-${LABEL_STAGE}"}}-{{(ds "config").debian_codename}}",
          "root-device-type": "ebs"
        },
        "owners": ["self"],
        "most_recent": true
      },
      "instance_type": "t3.small",
      "temporary_iam_instance_profile_policy_document": {{(ds "config").temporary_iam_instance_profile_policy_document}},
      "ssh_username": "admin",
      "ami_name": "jvb-{{getenv "LABEL_NAMESPACE"}}-{{getenv "LABEL_STAGE"}}-{{(ds "config").debian_codename}}",
      "ami_groups": ["all"],
      "run_tags": {
        "Project": "{{(ds "config").project}}",
        "Namespace": "{{getenv "LABEL_NAMESPACE"}}",
        "Stage": "{{getenv "LABEL_STAGE"}}"
      },
      "run_volume_tags": {
        "Project": "{{(ds "config").project}}",
        "Namespace": "{{getenv "LABEL_NAMESPACE"}}",
        "Stage": "{{getenv "LABEL_STAGE"}}"
      },
      "tags": {
        "Name": "jvb-{{env.ExpandEnv "${LABEL_NAMESPACE}-${LABEL_STAGE}"}}",
        "Date": "{{"{{"}}isotime | clean_resource_name{{"}}"}}",
        "OS": "debian/{{(ds "config").debian_codename}}",
        "Project": "{{(ds "config").project}}",
        "Namespace": "{{getenv "LABEL_NAMESPACE"}}",
        "Stage": "{{getenv "LABEL_STAGE"}}",
        "Application": "jvb"
      }
    }{{if  not (eq (sub (len (ds "config").regions) 1) $index) -}},{{ end -}}
{{ end }}
  ],
  "provisioners": [
    {
      "type": "ansible",
      "only": [
{{ range $index, $region := (ds "config").regions }}
      "debian-base-{{ $region }}"{{if  not (eq (sub (len (ds "config").regions) 1) $index) -}},{{ end -}}
{{ end }}
      ],
      "playbook_file": "{{(ds "config").ansible_path}}/playbooks/ami-debian-base.yml",
      "groups": ["packer", "debian-base"],
      "ansible_env_vars": [
        "ANSIBLE_PYTHON_INTERPRETER=/usr/bin/python3",
        "ANSIBLE_VARS_PLUGINS={{(ds "config").ansible_path}}/playbooks/plugins/vars",
        "ANSIBLE_ROLES_PATH={{(ds "config").ansible_path}}/playbooks/roles",
        "ANSIBLE_REMOTE_TMP=/tmp"
      ]
    },
    {
      "type": "ansible",
      "only": [
{{ range $index, $region := (ds "config").regions }}
      "jvb-{{ $region }}"{{if  not (eq (sub (len (ds "config").regions) 1) $index) -}},{{ end -}}
{{ end }}
      ],
      "playbook_file": "{{(ds "config").ansible_path}}/playbooks/ami-jitsi-videobridge.yml",
      "groups": ["packer", "jvb"],
      "ansible_env_vars": [
        "ANSIBLE_PYTHON_INTERPRETER=/usr/bin/python3",
        "ANSIBLE_VARS_PLUGINS={{(ds "config").ansible_path}}/playbooks/plugins/vars",
        "ANSIBLE_ROLES_PATH={{(ds "config").ansible_path}}/playbooks/roles",
        "ANSIBLE_REMOTE_TMP=/tmp"
      ]
    }
  ],
  "post-processors": [
    {
      "type": "manifest",
      "output" : "manifest.json",
      "strip_path": true
    }
  ]
}
